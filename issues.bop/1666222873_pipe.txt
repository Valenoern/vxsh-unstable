# "bashing"

a lisp shell should give you the ability to enter arbitrary lists of strings similar to e.g. "git push codeberg main"
but the idea should be that this /literally becomes a list of lisp symbols/ that maps to a function

in this way you could type into vxsh: bop publish targetname
and it would be the same as: (bopwiki:publish-bop "targetname")
or whatever particular thing it means.


in this way we can really start to tame parentheses specifically in the case of entering in ad-hoc scripts.
now one specific kind of nested function call can be turned into a unix-style pipe:

ш_  cat somefile | grep /words/g
really means something like:
(stem-utils:grep "/words/g" (stem-utils:cat somefile))
where likely 'grep further interprets "/words/g" into something like: (ppcre:create-scanner "/words/g")


likely the whole notion of "--argument"/"-a" would be discouraged,
in favour of every command taking a form like "git push codeberg main", or "git push codeberg :force" - which would evaluate to a form like: (git:push codeberg :force)


it's not necessary for vxsh to be "compatible with bash scripts", as much as just to allow an easier way to enter in lisp expressions which is /as intuitive/ as unix pipelines.
however there could be such a thing as "vxsh scripts", which are simply lisp files in this syntax that expand out to a list of lisp forms.

bashed syntax:
```
 #!ш
 # ^ a few synonyms: #!ш, #!vxsh, #!/bin/vxsh or some path like that
 # there has to be a pathless form in case you're using some non-unix lisp filing system.

 var-a="a string"
 var-b='(1 2 3 4)
 var-c=`reverse var-b`

 append var-b var-c | sort '<
```

interpreted lisp syntax:
```
(vxsh:shell-call
	(setq var-a "a string")
	(setq var-b '(1 2 3 4))
	(setq var-c (reverse var-b))
	(sort (append var-b var-c))  ; this value is returned if you "pipe" the script
	)
```

essentially, vxsh would be a kind of "lisp implementation" with a REPL, perhaps just sitting on top of sbcl and some things,
but 'ш would be a mode you could activate within it to run these simpler "shell scripts". you could pass a script into 'ш like an interpreter, or simply invoke it on the REPL for a more bash-style REPL   [1]

one could imagine that if "vxsh scripting" was finished, there could then be /a separate full port of bash/ which would attempt to be fully compatible with bash scripts.
in this way, "vxsh scripting" could be thought of like a step toward the bash port that deals with a subset of what the bash port would do.



[1] "but how do I type ш??" simple, we put it onto all compose keys as "<Multi_key> sh"   [*u]
no, there would be also a latin-letter alias to it if you need to call it manually. but the key is that the program would often call that function internally instead.

=> https://help.ubuntu.com/community/ComposeKey  *u. Compose Key ;
<= 1664938600 open
; cr. 1666222873
; LATER: pre is broken leaving an unescaped "#!" to turn into a heading. fix pre tags someday
