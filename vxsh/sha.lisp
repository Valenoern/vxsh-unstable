;;;; text scripting experiment {{{
(defpackage :vxsh.sha
	(:use :cl)
	(:local-nicknames
		(:g :grevillea)
			;; printl
	)
	(:export
		#:ш
))
(in-package :vxsh.sha)
;; }}}





(defun ш (shell-script) ; parser function "Sha", also called 'SH {{{
	;; this function takes in a shell script string and transforms it to lisp forms
	;; in the future, it might be nice to have a more "partial" one that can do autocompletion etc
	;; but for now it will simply transform a whole input
	
	(with-input-from-string (is shell-script)
		(format t "~S" (read-preserving-whitespace is))
		)
	
	;; LATER: this can be elaborated a lot. see issues.bop/1666222873 pipe
	
) ; }}}
