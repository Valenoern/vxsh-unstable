;;;; session shell experiment {{{
(defpackage :vxsh.session
	(:shadowing-import-from :grevillea
		#:printl
		#:letf)
	(:use :cl)
	(:local-nicknames
		(:g :grevillea)
			;; printl
		(:x :xylem)
			;; xylem-init xylem-value
	)
	(:export
		#:*session-tabs* #:*current-session*
		
		#:session #:make-session #:session-p
			;; SESSION is reused as a xylem key for storing sessions
		#:session-id #:session-name #:session-stack #:session-desk
		#:session-path
		
		#:get-session  ; (setf get-session)
		#:select-session #:list-sessions
		#:new-session
		#:save-session
		
		#:name-session #:pushd #:popd
		#:list-files
		
))
(in-package :vxsh.session)

(defparameter *session-tabs* '())
	;; a list that holds terminal/directory/workspace tabs
	;; as it's unlikely you'll need 20+ open /actively/, a regular list is fine
(defparameter *current-session* nil)
;; }}}

(defstruct (session)  ; id name stack desk  path {{{
	id     ; identifier of session while currently open
	name   ; friendly name
	stack  ; directory stack or 'history' of file listing
	desk   ; files which are 'open' in some broad sense
)

(defun session-path (session) ; most recent directory in session
	(letf
		(path  (car (session-stack session)))
		
		(if (null path)
			(uiop:getcwd)  path)
)) ; }}}


#| 1664938657_SHELL.txt
* directory listing
* from the session tab you can run some function or command to open files
* save session tab to disk
* reload session from disk

* LATER: icons for each file type, etc.
|#


;;; session list {{{

(defun get-session (query  &key id) ; {{{
	;; let each expression pair, in order
	(letf (same-format
		(equal (session-p query) (null id)))
	
	(setq
		query  ; if passed nothing in QUERY, use *CURRENT-SESSION*
			(if (null query)  *current-session*  query)
		
		query
		(if (or same-format (session-p query))
			query  ; if passed a SESSION or wanting :ID, do nothing
			(x:xylem-value (list 'session query))  ; otherwise look up session
			)
	)
	;; return ID if requested, otherwise whole session
	(if (or (null id) same-format)
		query
		(session-id query))
))

(defun (setf get-session) (session tab-id)
	(setf
		(x:xylem-value (list 'session tab-id))  session))
; }}}

(defun select-session (session) ; set session active {{{
	(setq session (get-session session :id t))  ; get session ID
	(setf *current-session* session)
) ; }}}

(defun list-sessions () ; show all loaded sessions {{{
	(dolist (session-id *session-tabs*)
		(letf (
			session (get-session session-id)  ; retrieve session
			id (session-id session)
			active  ; print asterisk if session is active
				(if (equal id *current-session*)
				"*"  " ")
			name
				(if (session-name session)
					(format nil "  ~a" (session-name session))
					"")
			)
		
		(format t "~A SESSION ~a~a ~a ~%"
			active  id name  (session-path session))
	))
	
	*session-tabs*  ; return list of session IDs
) ; }}}

(defun new-session () ; create new session tab struct {{{
	(letf (tab-id  (gentemp "S")
	       session (make-session :id tab-id))
	
	;; store session in xylem-value
	(when (null *session-tabs*)
		(x:xylem-init))
	(setf (get-session tab-id) session)
	
	;; if this is the first session tab, make it active
	(when (null *session-tabs*)
		(select-session session))
	
	(push tab-id *session-tabs*)  ; add session's ID to session list
	
	session
)) ; }}}

(defun save-session (&key id path) ; save session to disk {{{
	(letf
		(session  (get-session nil :id id)
		write-path
			(if (null path)
				(session-path session)
				path)
		write-file
			(merge-pathnames
				write-path
				(make-pathname :name "session" :type "sexp")))
		
		;; write session struct to file,
		;; in a format that should be able to be 'read
		(with-open-file
			(x write-file :direction :output)
			(format x "~s" session))
		
)) ; }}}

;;; }}}


(defun name-session (name  &optional session) ; change session display name {{{
	(setf (session-name (get-session session))  name)
	;; return new name
) ; }}}

(defun pushd (dir  &optional session) ; add most recent directory {{{
	;; add to session's directory stack, and return stack
	(push
		(uiop:ensure-pathname dir)
		(session-stack (get-session session)))
) ; }}}

(defun popd (&optional session) ; remove most recent directory {{{
	(setq session (get-session session))
	(let* (
		(last-element (pop (session-stack session)))  ; most recent directory - modifies STACK
		(stack (session-stack session))  ; directory stack
		)
		;; return directory stack and 'optionally' last element
		(values
			stack
			last-element)
)) ; }}}

(defun list-files (&optional session) ; {{{
	;; this lists only normal files
	;; LATER: show subdirs
	
	(letf ( 
		path  (session-path (get-session session))
		)
	
	(printl "session path" path)
	
	;;(uiop:directory-files )
)) ; }}}




;; uiop:run-program - ?
;; uiop:launch-program - run command in external window
